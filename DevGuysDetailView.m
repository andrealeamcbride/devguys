//
//  DevGuysDetailView.m
//  DevGuys
//
//  Created by Andrea McBride on 10/2/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
// hello

#import "DevGuysDetailView.h"

@implementation DevGuysDetailView

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.historyLabel.text = self.history;
    
   
  
}


#pragma mark UISplitViewControllerDelegate

-(void) awakeFromNib
{
    self.splitViewController.delegate = self;
}

-(BOOL) splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}


-(void) splitViewController: (UISplitViewController *) svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = @"VIPs";
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

-(void) splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    self.navigationItem.leftBarButtonItem = nil;
}



 
@end
