//
//  DevGuysDatabaseManager.h
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
// hello

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DevGuysDatabaseManager : UIViewController {
    sqlite3 *_database;
}

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *contactDB;
@property (strong, nonatomic) NSMutableArray *users;

 


-(void) openDB; 

 




@end
