//
//  DevGuysDatabaseManager.m
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
// hello

#import "DevGuysDatabaseManager.h"
#import "DevGuysUsers.h"

@implementation DevGuysDatabaseManager









- (id)init
{
    if (self = [super init])
        
        [self openDB];
    
    
    return self;
}



- (void)openDB
{
    
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:@"contacts.db"]];
    NSLog(@"db path is %@", _databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    
    //you must query the url each time for the date stored in the usersDict
    
    
    NSString *str = @"https://dl.dropboxusercontent.com/u/21621962/workspace.json";
    NSURL *url=[NSURL URLWithString:str];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSError *error;
    NSDictionary *usersDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    //JSON will return a string Format it to return a date
    NSString *remoteDateFull = [usersDict objectForKey:@"lastupdated"];
    
    NSRange remoteRange = NSMakeRange(0,
                                      remoteDateFull.length - 6 );
    NSString *remoteDate= [remoteDateFull substringWithRange:remoteRange];
    
    
    //convert date string to NSDate
    NSDateFormatter * Dateformats= [[NSDateFormatter alloc] init];
    
    [Dateformats setDateFormat:@"yyyy-MM-DD hh:mm:ss"]; //ex @"MM/DD/yyyy hh:mm:ss"
    
    //the date returned from JSON after formatting
    NSDate *myRemoteDate=[Dateformats dateFromString:remoteDate];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //locally stored date
    NSDate *localDate = [defaults objectForKey:@"localLastUpdate"];
    
    
    //get a past date for testing purposes
   // NSString *myDateString = @"Mon, 06 Sep 2009 16:45:00 -0900";
    
   // NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   // NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
  //  [dateFormatter setLocale:locale];
  //  [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss Z"];
    
   // localDate = [dateFormatter dateFromString:myDateString];
    
    //if there is no local date save a past date to NSUserDefaults
    if (!localDate) {
        NSString *myDateString = @"Mon, 06 Sep 2009 16:45:00 -0900";
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:locale];
        [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss Z"];
        
        localDate = [dateFormatter dateFromString:myDateString];
        
        
        
    }
    
    
    //  comparison will tell which is earlier/later/same:
    
    if ([localDate compare:myRemoteDate] == NSOrderedDescending) {
        NSLog(@"localDate is later than remoteDate dont update");
    } else if ([localDate  compare:myRemoteDate] == NSOrderedAscending) {
        NSLog(@"localDate is earlier than remoteDate update!");
    } else {
        NSLog(@"dates are the same");
    }
    
    if ([filemgr fileExistsAtPath: _databasePath ] == NO)
    {
        const char *dbpath = [_databasePath UTF8String];
        if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt =
            "CREATE TABLE IF NOT EXISTS CONTACTS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, POSITION TEXT, HISTORY TEXT, ICON TEXT)";
            
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog ( @"Failed to create table");
            }
            sqlite3_close(_contactDB);
        }
        else
        {
            NSLog ( @"Failed to open/create database");
        }
    }
    
    //if the database has changed re-write it
    if ([localDate  compare:myRemoteDate] == NSOrderedAscending)
    {
        [self deleteAllData ];
        self.users = [NSMutableArray array];
        for(NSDictionary *thisUser in [usersDict objectForKey:@"users"])
        {
            DevGuysUsers *thisUserObject = [[DevGuysUsers alloc] initWithJSONObject:thisUser];
            [self.users addObject:thisUserObject];
            
            //save to SQLite
            [self saveData: thisUserObject];
            
        }
        //save the last updated date locally
        [defaults setObject:myRemoteDate forKey:@"localLastUpdate"];
        
        
        [self devGuysInfos];
    }
    
    
    else
    {
        NSLog(@"pull from SQLite DB");
        [self devGuysInfos];
        
    }
    
}


- (NSArray *)devGuysInfos
{
    NSMutableDictionary *devDict = [[NSMutableDictionary alloc] init];
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        NSString *query = @"SELECT * FROM CONTACTS  ORDER BY name DESC";
        const char *sqlQuery = [query UTF8String];
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(_contactDB, sqlQuery, -1, &statement, NULL) == SQLITE_OK)
        {
            if(statement)
            {
                self.users = [NSMutableArray array];
            }
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                DevGuysUsers *newUser = [[DevGuysUsers alloc] init];
                newUser.name = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                newUser.position = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                newUser.history = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                newUser.icon = [NSURL URLWithString:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)]];
                [self.users addObject:newUser];
            }
            sqlite3_finalize(statement);
            DevGuysUsers *thisUserObject = [[DevGuysUsers alloc] initWithJSONObject:devDict];
            [self.users addObject:thisUserObject];
        }
        
        // Building select statement failed
        if (!statement) {
            NSAssert1(0, @"Can't build SQL to read Exercises [%s]", sqlite3_errmsg(_contactDB));
        }
        
    }
    
    return self.users;
    
}


- (void) saveData: (DevGuysUsers *) thisUser
{
    sqlite3_stmt *statement;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO CONTACTS (name, position, history, icon) VALUES (\"%@\", \"%@\", \"%@\", \"%@\")",
                               thisUser.name, thisUser.position, thisUser.history, thisUser.icon];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog ( @"Contact added");
        }
        else
        {
            NSLog (@"Failed to add contact");
        }
        sqlite3_finalize(statement);
        sqlite3_close(_contactDB);
    }
}

- (void) deleteAllData
{
    sqlite3_stmt *statement;
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        NSString *deleteQuery = @"DELETE FROM CONTACTS";
        const char *deleteChar = [deleteQuery UTF8String];
        sqlite3_prepare_v2(_contactDB, deleteChar, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog ( @"Deleted");
        }
        else
        {
           NSLog(@"Error while deleting data. '%s'", sqlite3_errmsg(_contactDB));
        }
        sqlite3_finalize(statement);
        sqlite3_close(_contactDB);
    }

}


@end
