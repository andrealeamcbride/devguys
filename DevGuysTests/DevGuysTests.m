//
//  DevGuysTests.m
//  DevGuysTests
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface DevGuysTests : XCTestCase

@end

@implementation DevGuysTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
