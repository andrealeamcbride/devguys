//
//  DevGuysUserTableViewCell.h
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DevGuysUserTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lb_name;
@property (weak, nonatomic) IBOutlet UILabel *lb_position;
@property (weak, nonatomic) IBOutlet UIImageView *img_profileImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
