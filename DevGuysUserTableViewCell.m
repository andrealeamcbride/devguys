//
//  DevGuysUserTableViewCell.m
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
//

#import "DevGuysUserTableViewCell.h"

@implementation DevGuysUserTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}



- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
