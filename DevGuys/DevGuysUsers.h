//
//  DevGuysUsers.h
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DevGuysUsers : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *history;
@property (nonatomic, strong) NSURL *icon;



-(id) initWithJSONObject:(NSDictionary *) thisUser;
-(void) getProfileImageWithCompletionHandler:(void (^) (UIImage *profileImage) ) completionHandler;


//- (void) refreshDataWith:(NSArray *) users;
@end
