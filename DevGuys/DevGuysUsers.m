//
//  DevGuysUsers.m
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
//

#import "DevGuysUsers.h"

@interface DevGuysUsers()
@property (nonatomic, strong) UIImage *profileImage;
@end

@implementation DevGuysUsers
@synthesize name = _name;
@synthesize position = _position;
@synthesize history = _history;
@synthesize icon = _icon;
@synthesize profileImage = _profileImage;

-(id) initWithJSONObject:(NSDictionary *) thisUser
{
    if(self = [super init])
    {
        _name = [thisUser valueForKey:@"name"];
        _position = [thisUser valueForKey:@"position"];
        _history = [thisUser valueForKey:@"history"];
        _icon = [NSURL URLWithString:[thisUser valueForKey:@"icon"]];
    }
    return self;
}

-(id) initWithName:(NSString *) name position: (NSString *) position {
    
    _name = name;
    _position = position;
    return self;
}

-(void) getProfileImageWithCompletionHandler:(void (^) (UIImage *profileImage) ) completionHandler
{
    if(!self.profileImage)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:self.icon];
            self.profileImage = [UIImage imageWithData:data];
            completionHandler(self.profileImage);
        });
        
        
        
        
        
    }
    else
        completionHandler(self.profileImage);
    
    
}

@end


