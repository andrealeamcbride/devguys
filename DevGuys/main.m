//
//  main.m
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DevGuysAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DevGuysAppDelegate class]));
    }
}
