//
//  DevGuysTableViewController.m
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
// hello

#import "DevGuysTableViewController.h"
#import "DevGuysUsers.h"
#import "DevGuysUserTableViewCell.h"
#import "DevGuysDatabaseManager.h"
#import <sqlite3.h>
#import "DevGuysDetailView.h"

@interface DevGuysTableViewController () 
@property (strong, nonatomic) NSMutableArray *users;
@property (strong, nonatomic) IBOutlet UILabel *historyLabel;
@property (strong, nonatomic) DevGuysDatabaseManager *dbmng;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


@implementation DevGuysTableViewController  {
    sqlite3 *_database;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1

    self.dbmng = [[DevGuysDatabaseManager alloc] init];
         self.users = self.dbmng.users;
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [self.tableView reloadData];
            
        });
    });

    
}






#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.users.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DevGuysUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VIP" forIndexPath:indexPath];
    
    // To grab the user fro array.
    DevGuysUsers *thisUser = self.users[indexPath.row];
    
    // To display appropriate data.
    [cell.lb_name setText: thisUser.name];
    [cell.lb_position setText:thisUser.position];
    [cell.activityIndicator setHidden:NO];
    [cell.activityIndicator startAnimating];
    
    [thisUser getProfileImageWithCompletionHandler:^(UIImage *profileImage) {
        [cell.activityIndicator stopAnimating];
        [cell.activityIndicator setHidden:YES];
        [cell.img_profileImage setImage:profileImage];
    }];
    
    
    return cell;
}

#pragma mark tableviewDelegateMethods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DevGuysUsers *thisUser = self.users[indexPath.row];
    [self.historyLabel setHidden:NO];
    self.historyLabel.text = thisUser.history;
    id detail = self.splitViewController.viewControllers [1];
    if ([detail isKindOfClass:[UINavigationController class]])
    {
        detail = [((UINavigationController*)detail).viewControllers firstObject];
        [self prepareDetailView:detail  withUser: thisUser];
    }
}

-(void) prepareDetailView: (DevGuysDetailView *)detailView withUser: (DevGuysUsers *) thisUser
{
   
    detailView.historyLabel.text = thisUser.history;
    detailView.position.text =thisUser.position;
    detailView.photoURL = thisUser.icon;
    detailView.nameTitle = thisUser.name;
    [thisUser getProfileImageWithCompletionHandler:^(UIImage *profileImage) {
       // [cell.activityIndicator stopAnimating];
        //[cell.activityIndicator setHidden:YES];
        [detailView.profileImage setImage:profileImage];
        
    }];
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.historyLabel setHidden:YES];
}




-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"editing");
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSLog(@"delete it");
        [self.users removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


 



@end
