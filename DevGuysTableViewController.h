//
//  DevGuysTableViewController.h
//  DevGuys
//
//  Created by Andrea McBride on 9/30/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
// hello

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface DevGuysTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *contactDB;

@property (strong, nonatomic) NSMutableArray *photoNSURL;

@end
