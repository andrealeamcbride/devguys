//
//  DevGuysDetailView.h
//  DevGuys
//
//  Created by Andrea McBride on 10/2/14.
//  Copyright (c) 2014 Andrea McBride. All rights reserved.
// hello

#import <UIKit/UIKit.h>

@interface DevGuysDetailView : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) NSString *history;
@property (strong, nonatomic) NSString *nameTitle; 


@property (weak, nonatomic) IBOutlet UILabel *position;

@property (weak, nonatomic) IBOutlet UILabel *historyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) NSURL *photoURL;

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@end
